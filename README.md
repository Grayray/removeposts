Step1. 設定把config.py中的mongo db 參數

Step2. 設定removeList
		groupId: 專頁id
		channelId: 頻道id
		day: 留下幾天的資料
		ex. removeList = [ {'groupId': '', 'channelId': '', 'day': 7},
			 	   {'groupId': '', 'channelId': '', 'day': 7}]

Step3. 設定要跑的function
		getChannelList(groupId)  #拿專頁裡所有的頻道

		countExistPost(removeList)  #頻道裡有多少文章
		 
		removePosts(removeList)    #刪頻道文章，只留50則文章（Default defaultPostNum = 50）
		
		removeSendPost(removeList)   #刪除頻道文章，只留7天的文章 (根據 removeList裡的day)
