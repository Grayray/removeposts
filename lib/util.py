# -*- coding: UTF-8 -*-
import os
import sys
import datetime
from time import time
import logging
import datetime
from os.path import dirname, abspath, join
from pymongo import MongoClient
from config import mongodb

logger = logging.getLogger('DelMlabData')

hdlr = logging.FileHandler('DelMlabData.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.DEBUG)

def getChannelList(group_id):
        logger.info('=== getChannelList ===')
        try:
                uri = mongodb['uri']
                client = MongoClient(uri)
                client[mongodb['db']].authenticate(mongodb['user'], mongodb['password'])

                db = client[mongodb['db']]
                channelCollection = db.Channel

                rr = channelCollection.find(
                        {
                                "groupId": group_id
                        }
                )
                for r in rr:
                        logger.info(r['_id'])
        except Exception as e:
                logger.error(e)
        finally:
                client.close()

def countExistPost(postList):
        logger.info('=== countExistPost ===')
        now = time()
        try:
                uri = mongodb['uri']
                client = MongoClient(uri)
                client[mongodb['db']].authenticate(mongodb['user'], mongodb['password'])

                db = client[mongodb['db']]
                postsCollection = db.Post
                for data in postList:
                        logger.info(data)
                        group = 'Group$' + data['groupId']
                        channel = 'Channel$' + data['channelId']

                        tmp = (now - 86400 * data['day'])
                        latestTimestamp = datetime.datetime.fromtimestamp(int(tmp))

                        rr = postsCollection.find(
                                {
                                        "_p_group": group,
                                        "_p_channel": channel,
                                        "onTop": False,
                                        "_created_at": {'$gt': latestTimestamp}
                                }
                        ).count()
                        logger.info('Exist Post Count: %s', rr)
                        
        except Exception as e:
                logger.error(e)
        finally:
                client.close()

def removePosts(removeList):
        logger.info('=== removePosts ===')
        defaultPostNum = 50
        now = time()
        try:
                uri = mongodb['uri']
                client = MongoClient(uri)
                client[mongodb['db']].authenticate(mongodb['user'], mongodb['password'])

                db = client[mongodb['db']]
                postsCollection = db.Post
                for data in removeList:
                        logger.info(data)
                        group = 'Group$' + data['groupId']
                        channel = 'Channel$' + data['channelId']

                        posts = postsCollection.find(
                                {
                                        "_p_group": group,
                                        "_p_channel": channel,
                                        "onTop": False
                                }
                        ).sort("_created_at")
                        postSize = posts.count()
                        
                        if postSize > defaultPostNum:
                                dueTimestamp = posts[postSize-defaultPostNum]['_created_at']

                                rr = postsCollection.remove(
                                        {
                                                "_p_group": group,
                                                "_p_channel": channel,
                                                "onTop": False,
                                                "_created_at": {'$lt': dueTimestamp}
                                        }
                                )

                                rr = postsCollection.find(
                                        {
                                                "_p_group": group,
                                                "_p_channel": channel,
                                                "onTop": False
                                        }
                                ).count()
                                if defaultPostNum == rr:
                                        logger.info('Delete defaultPostNum %s post successfully!', defaultPostNum)
                                else:
                                        logger.error('Delete post fail!')
                        else:
                                logger.error('Exist Post Count (less than defaultPostNum %d): %s', defaultPostNum, postSize)

        except Exception as e:
                logger.error(e)
        finally:
                client.close() 

def removeSendPost(removeList):
        logger.info('=== removeSendPost ===')
        now = time()
        try:
                uri = mongodb['uri']
                client = MongoClient(uri)
                client[mongodb['db']].authenticate(mongodb['user'], mongodb['password'])

                db = client[mongodb['db']]
                postsCollection = db.Post

                for data in removeList:
                        logger.info(data)
                        group = 'Group$' + data['groupId']
                        channel = 'Channel$' + data['channelId']

                        tmp = (now - 86400 * data['day'])
                        latestTimestamp = datetime.datetime.fromtimestamp(int(tmp))

                        rr = postsCollection.remove(
                                {
                                        "_p_group": group,
                                        "_p_channel": channel,
                                        "onTop": False,
                                        "_created_at": {'$lt': latestTimestamp}
                                }
                        )

                        logger.info('Delete %s day post successfully!', data['day'])

        except Exception as e:
                logger.error(e)
        finally:
                client.close()  